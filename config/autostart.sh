#!/bin/sh

#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 & eval $(gnome-keyring-daemon -s --components=pkcs11,secrets,ssh,gpg) &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &

sxhkd &
dunst &
xwallpaper --zoom $(find $HOME/Pictures/wall/ -type f | shuf -n 1) &
# xwallpaper --zoom $HOME/Pictures/wall/wallhaven-429p9n.png &
numlockx on &
picom --experimental-backends &
skippy-xd --start-daemon &
dwmblocks &
