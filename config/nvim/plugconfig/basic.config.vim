" Mouse Support
set mouse=a
" basics
let mapleader=" "
" set number relativenumber
set expandtab                   " Use spaces instead of tabs.
set smarttab                    " Be smart using tabs ;)
" set shiftwidth=4                " One tab == four spaces.
" set tabstop=4                   " One tab == four spaces.
set shiftwidth=2                " One tab == two spaces.
set tabstop=2                   " One tab == two spaces.

" Use system clipboard
set clipboard+=unnamedplus
set termguicolors
" colorscheme spaceway
let g:sonokai_enable_italic=1
let g:sonokai_transparent_background = 1
let g:sonokai_diagnostic_virtual_text = 'colored'
colorscheme sonokai
" colorscheme noirbuddy
" to make bg transparent
" hi Normal     guibg=NONE cterm=NONE gui=NONE
" hi EndOfBuffer     guibg=NONE cterm=NONE gui=NONE
" hi NonText     guibg=NONE cterm=NONE gui=NONE
highlight Normal ctermbg=NONE
highlight Conceal ctermbg=NONE

" Removes pipes | that act as seperators on splits
set fillchars+=vert:\ 

" Shortcutting split navigation
map <A-h> <C-w>h
map <A-j> <C-w>j
map <A-k> <C-w>k
map <A-l> <C-w>l

" Shortcut split opening
nnoremap <leader>h :split<Space>
nnoremap <leader>v :vsplit<Space>
nnoremap <silent> <leader>e :call ToggleNetrw()<cr>

" Fix splitting
set splitbelow splitright

" Autocompletion
set wildmode=longest,list,full

" GUI vim
set guifont=JetBrainsMono\ Nerd\ Font:h13.5
let g:neovide_transparency=1

" Vertically center the document when insert
" autocmd InsertEnter * norm zz

" Toggleing line numbers
let g:LineNumberIsShow=0
function! ToggleLineNumber()
    if g:LineNumberIsShow
        set nonumber
        set norelativenumber
        let g:LineNumberIsShow=0
    else
        set number relativenumber
        let g:LineNumberIsShow=1
    endif
endfunction

" Binding it
nnoremap <silent> <leader>n :call ToggleLineNumber()<cr>
let g:user_emmet_leader_key='<A-c>'

" Status line
set statusline=
" set statusline+=%#LineNr#
set statusline+=%#CursorColumn#
set statusline+=\ %f 
set statusline+=%=
set statusline+=%#CursorColumn#
set statusline+=\ %y
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
set statusline+=\[%{&fileformat}\]
set statusline+=\ %p%%
set statusline+=\ %l:%c
set statusline+=\ 

set list
set listchars=tab:→\ 

" Toggleing line numbers
let g:ShowInvChars=0
function! ToggleInvChars()
    if g:ShowInvChars
        set listchars=tab:→\ ,space:·,nbsp:␣,trail:•,eol:¶,precedes:«,extends:»
        let g:ShowInvChars=0
    else
        set listchars=tab:→\ ,space:·,nbsp:␣,trail:•
        let g:ShowInvChars=1
    endif
endfunction

set laststatus=0
" Toggleing statusline
let g:SLineIsShow=0
function! ToggleSLine()
    if g:SLineIsShow
        set laststatus=0
        let g:SLineIsShow=0
    else
        set laststatus=2
        let g:SLineIsShow=1
    endif
endfunction
nnoremap <silent> <leader>s :call ToggleSLine()<cr>

" Adding automatic loading for .vimrc
function! LoadProjectConfig()
    let vimFile = findfile(".vimrc", ".;")
    if !empty(vimFile)
        execute ":so" l:vimFile
        echom l:vimFile
    endif
endfunction
call LoadProjectConfig()
