" Toggleing destraction free mode 
let g:GoyoIsShow=0
function! ToggleGoyo()
    if g:GoyoIsShow
        Goyo!
        let g:GoyoIsShow=0
    else
        Goyo 120x100%
        let g:GoyoIsShow=1
    endif
endfunction
nnoremap <silent> <leader>d :call ToggleGoyo()<cr>
