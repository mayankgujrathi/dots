" sourceing configs
source ~/.config/nvim/plugconfig/basic.config.vim
" source ~/.config/nvim/plugconfig/mucomplete.vim
source ~/.config/nvim/plugconfig/easy-motion.vim
source ~/.config/nvim/plugconfig/netrw.vim
source ~/.config/nvim/plugconfig/goyo.vim
source ~/.config/nvim/plugconfig/lsp-config.vim
" source ~/.config/nvim/plugconfig/compe-config.lua
source ~/.config/nvim/plugconfig/cmp-config.vim
source ~/.config/nvim/plugconfig/fzf.vim
" source ~/.config/nvim/plugconfig/rust-lsp-config.lua
source ~/.config/nvim/plugconfig/java-lsp-config.lua
source ~/.config/nvim/plugconfig/term-config.lua
source ~/.config/nvim/plugconfig/treesitter.vim
