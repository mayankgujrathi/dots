autocmd BufRead,BufNewFile markrc set filetype=sh
autocmd BufRead,BufNewFile tsx,ts,js,jsx,rs set shiftwidth=2
autocmd BufRead,BufNewFile tsx,ts,js,jsx,rs set tabstop=2
autocmd BufRead,BufNewFile tsx,ts,js,jsx,rs set softtabstop=2
autocmd BufRead,BufNewFile tsx,ts,js,jsx,rs set expandtab
