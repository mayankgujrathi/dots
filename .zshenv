typeset -U PATH path

# Default Apps
export EDITOR="vi"
export READER="zathura"
export VISUAL="vi"
export TERMINAL="alacritty"
# export TERMINAL="st"
export BROWSER="firefox"
export BROWSER_PVT="firefox -private-window"
# export BROWSER="brave"
export VIDEO="mpv"
export IMAGE="sxiv"
export COLORTERM="truecolor"
export OPENER="xdg-open"
export PAGER="less"

# Other XDG paths
export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}

# Doesn't seem to work (android)
# export ANDROID_SDK_HOME="$XDG_CONFIG_HOME"/android
# export ANDROID_AVD_HOME="$XDG_DATA_HOME"/android
# export ANDROID_EMULATOR_HOME="$XDG_DATA_HOME"/android
# export ADB_VENDOR_KEY="$XDG_CONFIG_HOME"/android
# export GRADLE_USER_HOME="$XDG_DATA_HOME"/gradle
export ANDROID_HOME="$HOME/Android/Sdk"

# Disable files
export LESSHISTFILE=-
# ruby
export GEM_PATH="$XDG_DATA_HOME/ruby/gems"
export GEM_SPEC_CACHE="$XDG_DATA_HOME/ruby/specs"
export GEM_HOME="$XDG_DATA_HOME/ruby/gems"
export BUNDLE_USER_CONFIG="$XDG_CONFIG_HOME"/bundle
export BUNDLE_USER_CACHE="$XDG_CACHE_HOME"/bundle
export BUNDLE_USER_PLUGIN="$XDG_DATA_HOME"/bundle
# npm && nodejs
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc
export NODE_REPL_HISTORY="$XDG_DATA_HOME"/node_repl_history
# go
export GOPATH="$XDG_DATA_HOME"/go
# gpg
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
# parallel
export PARALLEL_HOME="$XDG_CONFIG_HOME"/parallel
# java
# export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
# gtk
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
# zsh
export ZDOTDIR=$HOME/.config/zsh
# rust
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup
# vscode
export VSCODE_PORTABLE="$XDG_DATA_HOME"/vscode
# docker
export DOCKER_CONFIG="$XDG_CONFIG_HOME"/docker
export MACHINE_STORAGE_PATH="$XDG_DATA_HOME"/docker-machine
# kde
export KDEHOME="$XDG_CONFIG_HOME"/kde
# virtuals
export WORKON_HOME="$XDG_DATA_HOME/virtualenvs"
export VAGRANT_HOME="$XDG_DATA_HOME"/vagrant
export VAGRANT_ALIAS_FILE="$XDG_DATA_HOME"/vagrant/aliases
# conda
export CONDARC="$XDG_CONFIG_HOME/conda/condarc"
# wine
# export WINEPREFIX="$XDG_DATA_HOME"/wineprefixes/default
# zoom
export SSB_HOME="$XDG_DATA_HOME"/zoom
# kodi
export KODI_DATA="$XDG_DATA_HOME"/kodi
# sqlite
export SQLITE_HISTORY="$XDG_CACHE_HOME"/sqlite_history
# command -v sqlite3 > /dev/null && sqlite3 -init "$XDG_CONFIG_HOME"/sqlite3/sqliterc
# zsh suggestion
export ZSH_AUTOSUGGEST_STRATEGY="completion"
# flutter chrome
export CHROME_EXECUTABLE=~/.local/apps/chromium
# wayland fix
export QT_QPA_PLATFORM=wayland

# Path
path=("$HOME/.local/bin" "$HOME/.local/apps" "$XDG_DATA_HOME/mark/bin" "$HOME/.local/apps/nodejs/bin" "$HOME/.local/apps/android-studio/bin" "$HOME/.local/apps/flutter/bin" "$HOME/.local/apps/Discord" "$HOME/.local/apps/tex/installed/bin/x86_64-linux" "$CARGO_HOME/bin" "$path[@]")
export PATH
export FZF_DEFAULT_OPTS="--layout=reverse --height 40%"

MANPATH="$MANPATH:/home/mayank/.local/apps/tex/installed/texmf-dist/doc/man"
INFOPATH="$INFOPATH:/home/mayank/.local/apps/tex/installed/texmf-dist/doc/info"

[ -n "$XDG_DATA_HOME" ] && source "$XDG_CONFIG_HOME/self/markrc"
