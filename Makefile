BASE = $(PWD)
TMP_DIR = $(BASE)/tmp
SU_TOOL = doas

PKG_IN = $(SU_TOOL) pacman -S
PKG_INO = $(SU_TOOL) pacman -S --needed
PKG_OUT = $(SU_TOOL) pacman -Rns
AUR_IN =  $(BASE)/local/bin/aur
AUR_OUT = $(BASE)/local/bin/aur -r
LN = ln -svf
RM = rm -rf
APPS_DIR=${HOME}/.local/apps
SAMPLE_APPIMAGE=$(BASE)/local/src/sampleappimage
# coreutils list
CORE_UTILS_PKG = fd exa bat entr fzf dunst polkit-gnome xwallpaper xclip zathura zathura-pdf-poppler mpv youtube-dl maim numlockx dash pamixer pulseaudio imagemagick
CORE_UTILS_AUR = pup-git aria2-git archiver devour nerd-fonts-jetbrains-mono dashbinsh boomer-git
# themes and fonts
THEME_PKG = materia-gtk-theme papirus-icon-theme lxappearance qt5ct ttf-joypixels capitaine-cursors
THEME_AUR = skippy-xd-git

# for appimage
# pacman -S fuse2
# for vulkan
# pacman -S vulkan-intel
# for Davinci Resolve
# pacman -S intel-compute-runtime
# LaTex / Tex Installs
# https://tug.org/texlive/quickinstall.html

# settings
.DEFAULT_GOAL := help

help: ## Prints help message and exits
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) \
	| sort \
	| awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

pushable: ## Make dots ready for git commits
	make clean -C $(BASE)/config/zsh/plugs/
	make clean -C $(BASE)/config/nvim/pack/plugins/start
	$(RM) $(BASE)/config/nvim/.netrwhist
	$(RM) $(BASE)/config/zsh/.zcompdump

usable: ## Make dots ready to use locally
	make install -C $(BASE)/config/zsh/plugs/
	make install -C $(BASE)/config/nvim/pack/plugins/start

core: zsh alacritty aur self lightdm scripts tmux ## Makes coreutils for system
	$(PKG_INO) $(CORE_UTILS_PKG)
	$(AUR_IN) $(CORE_UTILS_AUR)

theme:
	$(PKG_INO) $(THEME_PKG)
	$(AUR_IN) $(THEME_AUR)
	gsettings set org.gnome.desktop.interface gtk-theme 'Materia-dark-compact'
	gsettings set org.gnome.desktop.interface icon-theme 'Papirus'
	gsettings set org.gnome.desktop.interface cursor-theme 'capitaine-cursors-light'

# CUSTOM PROGRAMS #
nodejs: ## Makes nodejs from nodejs.org require $INSTALL_PKG_DIR
	mkdir -p $(TMP_DIR)
	$(BASE)/sh/nodejs.sh $(TMP_DIR) ${INSTALL_PKG_DIR}
	rm -rf $(TMP_DIR)

cpy: ## Makes Anaconda python require $INSTALL_PKG_DIR
	mkdir -p $(TMP_DIR)
	$(BASE)/sh/cpy.sh $(TMP_DIR) ${INSTALL_PKG_DIR}
	rm -rf $(TMP_DIR)

virtualbox: ## Installs latest Virtualbox
	mkdir -p $(TMP_DIR)
	$(BASE)/sh/virtualbox.sh $(TMP_DIR)
	rm -rf $(TMP_DIR)

blender: ## Installs latest blender as Appimage
	mkdir -p $(TMP_DIR)
	$(BASE)/sh/blender.sh $(TMP_DIR) $(SAMPLE_APPIMAGE) $(APPS_DIR)
	rm -rf $(TMP_DIR)

rust_analyzer: ## Installs latest rust-analyzer
	$(BASE)/sh/rust_analyzer.sh $(APPS_DIR)

heroic: ## Installs latest heroic
	$(BASE)/sh/heroic.sh $(APPS_DIR)

libreoffice: ## Installs fresh libreoffice
	$(BASE)/sh/libreoffice.sh $(APPS_DIR)

tailwind: ## Installs latest tailwind
	$(BASE)/sh/tailwind.sh $(APPS_DIR)

figma: ## Installs latest figma
	$(BASE)/sh/figma.sh $(APPS_DIR)

trunk: ## Installs latest trunk
	mkdir -p $(TMP_DIR)
	$(BASE)/sh/trunk.sh $(TMP_DIR) $(APPS_DIR)
	rm -rf $(TMP_DIR)

chromium: ## Installs latest ungoogledchromium
	$(BASE)/sh/ungooglechromium.sh $(APPS_DIR)

tex: ## Makes texlive
	$(BASE)/sh/tex.sh $(APPS_DIR)

qemu: ## Installs latest qemu as Appimage
	$(PKG_INO) ninja
	mkdir -p $(TMP_DIR)
	$(BASE)/sh/qemu.sh $(TMP_DIR) $(SAMPLE_APPIMAGE) $(APPS_DIR)
	rm -rf $(TMP_DIR)
	$(PKG_OUT) ninja

# Custom Libs #
libxft:
	$(AUR_IN) libxft-bgra
	libtool --finish /usr/lib

# CORE APPS CONFS #
zsh: ## Installs zsh
	$(PKG_INO) zsh
	$(RM) ~/.zshenv
	$(LN) $(BASE)/.zshenv ~/.zshenv
	mkdir -p ~/.config/
	$(RM) ~/.config/zsh
	$(LN) $(BASE)/config/zsh ~/.config/zsh

alacritty:
	$(PKG_INO) alacritty
	$(RM) ~/.config/alacritty
	$(LN) $(BASE)/config/alacritty ~/.config/alacritty

neovim:
	$(PKG_INO) neovim
	mkdir -p ~/.config/
	$(RM) ~/.config/nvim
	$(LN) $(BASE)/config/nvim ~/.config/nvim
	make usable

sxiv:
	$(PKG_INO) sxiv
	$(RM) ~/.config/sxiv
	$(LN) $(BASE)/config/sxiv ~/.config/sxiv

sxhkd:
	$(PKG_INO) sxhkd
	$(RM) ~/.config/sxhkd
	$(LN) $(BASE)/config/sxhkd ~/.config/sxhkd

qutebrowser:
	$(PKG_INO) qutebrowser
	$(RM) ~/.config/qutebrowser
	$(LN) $(BASE)/config/qutebrowser ~/.config/qutebrowser

dunst:
	$(PKG_INO) dunst
	$(RM) ~/.config/dunst
	$(LN) $(BASE)/config/dunst ~/.config/dunst

picom:
	$(AUR_IN) picom-jonaburg-git
	$(RM) ~/.config/picom
	$(LN) $(BASE)/config/picom ~/.config/picom

tmux:
	$(PKG_INO) tmux
	$(RM) ~/.config/tmux
	$(LN) $(BASE)/config/tmux ~/.config/tmux

rofi:
	$(PKG_INO) rofi
	$(RM) ~/.config/rofi
	$(LN) $(BASE)/config/rofi ~/.config/rofi

self:
	$(RM) ~/.config/self
	$(LN) $(BASE)/config/self ~/.config/self

aur:
	$(RM) ~/.config/aur
	$(LN) $(BASE)/config/aur ~/.config/aur

scripts:
	# cp -r $(BASE)/local/bin/* ~/.local/bin/
	$(RM) ~/.local/bin
	$(LN) $(BASE)/local/bin ~/.local/bin

mime:
	cp -r $(BASE)/./local/share/application/* $(HOME)/.local/share/applications
	$(PKG_INO) xdg-utils
	$(BASE)/sh/mime.sh

vscodium:
	$(AUR_IN) vscodium-bin
	mkdir -p $(HOME)/.config/VSCodium/User
	$(RM) $(HOME)/.config/VSCodium/User/settings.json
	$(RM) $(HOME)/.config/VSCodium/User/keybindings.json
	$(LN) $(BASE)/config/VSCodium/User/settings.json $(HOME)/.config/VSCodium/User/settings.json
	$(LN) $(BASE)/config/VSCodium/User/keybindings.json $(HOME)/.config/VSCodium/User/keybindings.json

bluez:
	$(PKG_INO) bluez bluez-utils pulseaudio-bluetooth r8168
	$(SU_TOOL) systemctl enable bluetooth

lightdm: ## Makes Lightdm with my settings
	$(PKG_INO) cantarell-fonts lightdm lightdm-gtk-greeter
	$(SU_TOOL) mkdir -p /usr/share/backgrounds/
	$(SU_TOOL) cp $(BASE)/usr/share/backgrounds/image.png /usr/share/backgrounds/image.png
	$(SU_TOOL) cp $(BASE)/usr/share/backgrounds/user-icon.png /usr/share/backgrounds/user-icon.png
	[ -e "/etc/lightdm/lightdm-gtk-greeter.conf" ] && $(SU_TOOL) cp /etc/lightdm/lightdm-gtk-greeter.conf "/etc/lightdm/lightdm-gtk-greeter.conf.$(date)"
	$(SU_TOOL) cp $(BASE)/etc/lightdm/lightdm-gtk-greeter.conf /etc/lightdm/lightdm-gtk-greeter.conf
	[ -e "/etc/lightdm/lightdm.conf" ] && $(SU_TOOL) cp /etc/lightdm/lightdm.conf "/etc/lightdm/lightdm.conf.`date +%s`"
	$(SU_TOOL) cp $(BASE)/etc/lightdm/lightdm.conf.example /etc/lightdm/lightdm.conf
	@echo "[NOTE] Please enable lightdm service, to see changes!!"
