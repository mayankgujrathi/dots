pub struct Mark {
    pub comment: String,
    pub interpreter: String,
    pub name: String,
    pub content: String,
}

impl Mark {
    pub fn make(&self) -> std::io::Result<()> {
        use std::fs;
        use std::io::prelude::*;
        use std::os::unix::fs::PermissionsExt;

        let base_dir = match std::env::var("XDG_DATA_HOME") {
            Ok(path) => format!("{path}/mark/bin"),
            _ => "~/.local/share/mark/bin".to_string(),
        };
        let xname = format!("*{}", self.name);
        let mut content = String::new();
        if self.content.contains(&xname) {
            let new_path = match self.path_for_name(&base_dir) {
                Some(path) => path,
                None => self.name.clone(),
            };
            content = self.content.replace(&xname, &new_path);
        }

        fs::create_dir_all(&base_dir)?;
        let file_name = format!("{base_dir}/{}", self.name);
        let mut file = fs::File::create(file_name)?;
        file.write_fmt(format_args!(
            "{}!{}\n{}",
            self.comment,
            self.interpreter,
            if content.is_empty() {
                self.content.clone()
            } else {
                content
            }
        ))?;
        let mut prems = file.metadata()?.permissions();
        prems.set_mode(0o711);
        file.set_permissions(prems)?;
        Ok(())
    }
    pub fn clean() -> std::io::Result<()> {
        let base_dir = match std::env::var("XDG_DATA_HOME") {
            Ok(path) => format!("{path}/mark/bin"),
            _ => "~/.local/share/mark/bin".to_string(),
        };
        std::fs::remove_dir_all(&base_dir)?;
        Ok(())
    }
    fn path_for_name(&self, base_dir: &String) -> Option<String> {
        let paths = env!("PATH");
        let paths = paths.split(":");
        for path in paths {
            if path != base_dir {
                let expected_path = format!("{path}/{}", self.name);
                if std::path::Path::new(&expected_path).exists() {
                    return Some(expected_path);
                }
            }
        }
        None
    }
}
