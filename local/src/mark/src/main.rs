mod mark;

fn main() {
    use mark::Mark;
    use std::io::BufRead;

    let args: Vec<String> = std::env::args().collect();
    if args.len() == 2 && args[1] == "--clean" {
        Mark::clean().unwrap();
        return;
    }
    let mut args = args.iter();
    args.next().unwrap();
    let mut interpreter = "/bin/sh".to_string();
    let mut comment = "#".to_string();
    let mut script = "".to_string();
    let mut stdin_script = false;
    let mut skip_args = false;
    let mut mark_name = "".to_string();
    for arg in args {
        if !skip_args && arg == "--stdin" || arg == "-i" {
            stdin_script = true;
        } else if !skip_args && arg.starts_with("-c") {
            comment = arg.replace("-c", "");
        } else if !skip_args && arg.starts_with("--") {
            interpreter = arg.replace("--", "");
        } else if !skip_args && arg.starts_with(":") {
            mark_name = arg.replacen(":", "", 1);
            skip_args = true;
        } else {
            script = format!("{script}{arg} ");
        }
    }
    // read script properly
    if stdin_script {
        let stdin = std::io::stdin();
        let buf = stdin.lock();
        script += "\n";
        for line in buf.lines() {
            script = format!("{script}{}\n", line.unwrap());
        }
    }
    let marker = Mark {
        interpreter,
        comment,
        name: mark_name,
        content: script.replace("\\$", "$"),
    };
    marker.make().unwrap();
}
