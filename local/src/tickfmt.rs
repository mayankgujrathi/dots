use std::{collections::HashMap, env};

const N: u64 = 1;
const MS: u64 = N * 10i32.pow(6) as u64;
const S: u64 = MS * 10i32.pow(3) as u64;
const M: u64 = S * 60;
const H: u64 = M * 60;
const D: u64 = H * 24;

fn fmt(s: u64, e: u64) -> String {
    let mut map = HashMap::from([("n", 0), ("ms", 0), ("s", 0), ("m", 0), ("h", 0), ("d", 0)]);
    let mut dt = e - s;
    while dt > 0 {
        if dt >= D {
            map.entry("d").and_modify(|c| { *c += 1 });
            dt -= D;
        } else if dt >= H {
            map.entry("h").and_modify(|c| { *c += 1 });
            dt -= H;
        } else if dt >= M {
            map.entry("m").and_modify(|c| { *c += 1 });
            dt -= M;
        } else if dt >= S {
            map.entry("s").and_modify(|c| { *c += 1 });
            dt -= S;
        } else if dt >= MS {
            map.entry("ms").and_modify(|c| { *c += 1 });
            dt -= MS;
        } else if dt >= N {
            map.entry("n").and_modify(|c| { *c += 1 });
            dt -= N;
        } else {
            println!("{dt}");
            unreachable!();
        }
    }
    let mut ret = String::new();
    if map["d"] != 0 {
        ret = format!("{}d ", map["d"]);
    }
    if map["h"] != 0 {
        ret = format!("{ret}{}h ", map["h"]);
    }
    if map["m"] != 0 {
        ret = format!("{ret}{}m ", map["m"]);
    }
    if map["s"] != 0 {
        ret = format!("{ret}{}s ", map["s"]);
    }
    if map["ms"] != 0 {
        ret = format!("{ret}{}ms ", map["ms"]);
    }
    if map["n"] != 0 {
        ret = format!("{ret}{}n", map["n"]);
    }
    ret
}

fn main() {
    let mut args = env::args().collect::<Vec<String>>().into_iter();
    args.next().unwrap();
    let start = args.next().expect("E: Expected start time").parse::<u64>().expect("E: Invalid u64");
    let end = args.next().expect("E: Expected end time").parse::<u64>().expect("E: Invalid u64");
    println!("{}", fmt(start, end));
}
