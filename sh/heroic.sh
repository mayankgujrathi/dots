#!/bin/sh
# $1: out_dir
set -e
set -x

[ -d "$1" ] || exit 1
url="$(curl -s "https://api.github.com/repos/Heroic-Games-Launcher/HeroicGamesLauncher/releases/latest" | grep -i 'browser_download_url.*appimage\"$' | cut -d '"' -f 4)"

aria2c -x 16 -u 1K "$url" -d "$1" -o "heroic" && chmod +x "$1/heroic"
