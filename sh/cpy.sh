#!/bin/sh
# required $tmp_dir: valid_path $install_dir: vaild_path
set -e
set -x

[ -d "$1" ] || exit 1
[ -d "$2" ] || exit 2

url=$(curl -s https://www.anaconda.com/products/individual | pup 'a.link-download.js-event-individual attr{href}' | grep -i 'Linux-x86_64')
file_name=$(echo -e $(echo "$url" | sed 's/\//\\n/g') | tail -1)
dl_path="$1/$file_name"

aria2c -x 16 -u 1K "$url" -d "$1" -o "$file_name"
# curl "$url" > "$dl_path"
sh "$dl_path" -f -u -b -p "$2/cpy"
