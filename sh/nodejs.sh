#!/bin/sh
# required $tmp_dir: valid_path $install_dir: valid_path
set -e
set -x

[ -d "$1" ] || exit 1
[ -d "$2" ] || exit 2

url=$(curl -s https://nodejs.org/en/download | pup 'a attr{href}' | grep -i 'linux-x64.tar.xz')
file_name=$(echo $(echo "$url" | sed 's/\//\\n/g') | tail -1)
opened_dir_name=$(echo $(echo "$file_name" | sed 's/\.tar\.xz//g') | tail -1)
dl_path="$1/$file_name"

aria2c -x 16 -u 1K "$url" -d "$1" -o "$file_name"
arc unarchive "$dl_path" "$1"
rm -rf "$2/nodejs"
mv "$1/$opened_dir_name" "$2/nodejs"
