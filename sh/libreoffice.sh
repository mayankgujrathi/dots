#!/bin/sh
# $1: out_dir
set -e
set -x

[ -d "$1" ] || exit 1
url="$(curl -s "https://appimage.sys42.eu/" | pup 'a attr{href}' | grep '.*AppImage$' | grep '.*fresh.*full.*x86_64.*' | head -n 1)"

aria2c -x 16 -u 1K "https://appimage.sys42.eu/$url" -d "$1" -o "libreoffice" && chmod +x "$1/libreoffice"
