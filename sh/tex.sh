#!/bin/sh
# $install_dir: valid_path
set -e
set -x

[ -d "$1" ] || exit 1

url=$(curl -s "https://www.tug.org/texlive/quickinstall.html" | pup 'ol li tt + a attr{href}')
file_name=$(echo $(echo "$url" | sed 's/\//\\n/g') | tail -1)
mkdir -p "$1/tex/installed"
dl_path="$1/tex/$file_name"

aria2c -x 16 -u 1K "$url" -d "$1/tex" -o "$file_name"
arc unarchive "$dl_path" "$1/tex"
dir_name="$(find "$1/tex" -iname 'install-tl-*' -type d)"
cd "$dir_name"
perl ./install-tl --no-interaction --texdir="$1/tex/installed" --no-interaction --paper=a4
rm -rf "$dir_name" "$dl_path"
