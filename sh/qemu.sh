#!/bin/sh
# $1: tmp_dir $2: sample_app $3: out_dir
set -ex

# setup sample_app
cp "$2" -r "$1/vm"
sed -i -e 's/test/vm/g' "$1/vm/Makefile"
rm -f "$1/vm/AppDir/test.sh"
mv "$1/vm/AppDir/test.desktop" "$1/vm/AppDir/vm.desktop"
sed -i -e 's/test/vm/g' "$1/vm/AppDir/vm.desktop"
cp "$2/../qemu/sample.AppRun" "$1/vm/AppDir/AppRun"

# downloading src
base_url="https://download.qemu.org"
filename="$(curl -L -s "$base_url"\
    | pup 'tbody tr a text{}'\
    | grep '^qemu.*tar.xz'\
    | tail -2\
    | head -1\
    | sed -e 's/\.sig//g')"
filename_noext="$(echo $filename | sed -e 's/\.tar.xz//g')"
url="$base_url/$filename"
aria2c -x 16 -d "$1" -o "$filename" "$url"
arc unarchive "$1/$filename" "$1"
cd "$1/$filename_noext"

# compile
./configure
# make -j "$(($(nproc)/2))"
make -j 3

# create app
mv ./build "$1/vm/AppDir/bin"
rm -rf $1/vm/AppDir/bin/python
mv ./python "$1/vm/AppDir/bin/python"
rm -rf $1/vm/AppDir/bin/scripts
mv ./scripts "$1/vm/AppDir/bin/scripts"
mv ./.gdbinit "$1/vm/AppDir/bin/.gdbinit"
mv ./Makefile "$1/vm/AppDir/bin/Makefile"
cd "$1/vm"
make
mv ./vm "$3/vm"
