#!/bin/sh
# $1: out_dir

curl -L "https://github.com/rust-analyzer/rust-analyzer/releases/latest/download/rust-analyzer-x86_64-unknown-linux-gnu.gz"\
   | gunzip -c - > "$1/rust-analyzer"\
   && chmod +x "$1/rust-analyzer"
