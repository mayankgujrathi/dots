#!/bin/sh
# required $tmp_dir: valid_path $install_dir: valid_path
set -e
set -x

[ -d "$1" ] || exit 1
[ -d "$2" ] || exit 2
url="$(curl -s "https://api.github.com/repos/thedodd/trunk/releases/latest" | grep -i 'browser_download_url.*linux.*tar.gz\"$' | cut -d '"' -f 4)"
file_name=$(echo $(echo "$url" | sed 's/\//\\n/g') | tail -1)
dl_path="$1/$file_name"

aria2c -x 16 -u 1K "$url" -d "$1" -o "$file_name"
arc unarchive "$dl_path" "$1"
mv "$1/trunk" "$2/trunk"
