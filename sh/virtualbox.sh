#!/bin/sh
# required $tmp_dir: valid_path
set -e
set -x

[ -d "$1" ] || exit 1

url=$(curl -s https://www.virtualbox.org/wiki/Linux_Downloads | pup 'a.ext-link attr{href}' | grep '.run$')
file_name=$(echo -e $(echo "$url" | sed 's/\//\\n/g') | tail -1)
dl_path="$1/$file_name"

aria2c -x 16 -u 1K "$url" -d "$1" -o "$file_name"
# curl "$url" > "$dl_path"
pkexec sh "$dl_path"
