#!/bin/sh
# require $1:tmp dir, $2:sample_appimage_dir $3: dist_dir
set -e
set -x

# url=$(curl -s "https://builder.blender.org/download/daily/" | pup 'li.os.linux a.js-ga attr{href}' | grep -e "candidate.*-linux.x86_64-release.tar.xz$" | tail -1)
url=$(curl -H "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:100.0) Gecko/20100101 Firefox/100.0" -s "$(curl -H "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:100.0) Gecko/20100101 Firefox/100.0" -s "https://www.blender.org/download/" | pup "html body.ps-download.has-header div.container-main section.featured.download.header-align-x-center.header-align-y-center div.container div.featured-content div#linux.dl-header-cta.dl-os-linux.active a.btn.px-5.js-download.dl-header-cta-button attr{href}")" | pup "html body.ps-thanks.has-header.is-scrolled.is-navbar-hidden div.container-main div.container div.row.text-center div.col-md-8.mx-auto div.box.m-4.px-4 p.text-muted a attr{href}")
aria2c -x 16 -u 1K "$url" -d "$1" -o "blender.tar.xz"
cp -r "$2" "$1/app"

arc unarchive "$1/blender.tar.xz" "$1/blender"
hidden_dir_name=$(tar --list -f "$1/blender.tar.xz" | head -1 | cut -d '/' -f 1)
mv "$1"/blender/"$hidden_dir_name"/* "$1/app/AppDir"

sed -i -e "s/test/Blender/g" "$1/app/Makefile"
sed -i -e "s/test.sh/blender/g" "$1/app/AppDir/AppRun"
rm -f "$1/app/AppDir/test.sh" "$1/app/AppDir/test.desktop" "$1/app/AppDir/test.sh"
make -C "$1/app"

mv "$1/app/Blender" "$3/blender"
