#!/bin/sh
set -ex

text_list="
text/plain
text/x-c
text/x-cpp
text/x-h
text/x-hpp
inode/x-empty
application/json
application/xml
text/xml
text/html
"
printf "$text_list" | xargs -rI % xdg-mime default text.desktop %
