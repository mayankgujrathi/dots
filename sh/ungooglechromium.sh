#!/bin/sh
# $1: out dir
set -ex

b_url="https://ungoogled-software.github.io"
hasx="$(date +'%s')"
out_file="chromium"
out_path="$1/$out_file"

url=$(curl -s "$b_url$(curl -s "$b_url/ungoogled-chromium-binaries/"\
| pup 'table tr td a attr{href}'\
| grep 'appimage'\
| tail -1)"\
| pup 'ul li a attr{href}'\
| grep -i '.appimage$')

aria2c -x 16 -u 1K -d "$1" -o "$out_file.$hasx" "$url" && mv "$1/$out_file.$hasx" "$out_path" && chmod +x "$out_path"
